import React from 'react';
import { data } from './JSON/json'
import FoldersView from './FoldersView/FoldersView'
import './App.css';

function App() {
  
  const files = data.children.map((file) => {
    return file.name
  })
  return (
    <div className="App">
      <FoldersView />
    </div>
  );
}

export default App;
