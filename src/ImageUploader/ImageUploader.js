import React, { Component } from 'react';
import axios from 'axios';
import { MdDelete } from "react-icons/md"
import './ImageUploader.css';

class ImageUploader extends Component {

  state = {
    selectedFile: null,
    selectedFiles: []
  };

  
  onFileChange = event => {
    this.setState({ selectedFile: event.target.files[0] });
  };

  onFileUpload = (event) => {
    event.preventDefault()
    const formData = new FormData();

    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    this.setState({
      selectedFiles: [...this.state.selectedFiles, this.state.selectedFile]
    })

    // Details of the uploaded file 
    console.log(this.state.selectedFiles);
    //axios.post("api/uploadfile", formData);
  };

  deleteFile = (index) => {
    let selectedFiles = [...this.state.selectedFiles]
    if (index !== -1) {
      selectedFiles.splice(index, 1);
      this.setState({selectedFiles: selectedFiles});
    }
  }

  

  render() {
    const { selectedFiles } = this.state
    return (
      <div>
        <form onSubmit={(event) => this.onFileUpload(event)}>
          <div className="uploadHeader">Select image to upload:</div>
          <input type="file" name="fileToUpload" id="fileToUpload" onChange={this.onFileChange} />
          <input type="submit" value="Upload Image" name="submit" />
        </form>
        {/* <table className="styled-table">
          <thead>
            <tr>
              <th>S.No</th>
              <th>File Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {selectedFiles && selectedFiles.length > 0 ? selectedFiles.map((file, index) => {
              return <tr key={index}>
                <td>{index + 1}</td>
                <td>{file.name}</td>
                <td><MdDelete className="deleteIcon" onClick={() => this.deleteFile(index)} /></td>
              </tr>
            }) : <tr>
              <td></td>
              <td>No Files Available</td>
              <td></td>
            </tr>}
          </tbody>
        </table> */}
        {/* {this.fileData()} */}
      </div>
    );
  }
}

export default ImageUploader;