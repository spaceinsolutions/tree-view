export const data = {
    "type": "directory",
    "name": "Root",
    "children": [
        {
            "type": "directory",
            "name": "Folder1",
            "children": [
                {
                    "type": "file",
                    "name": "one.txt"
                },
                {
                    "type": "file",
                    "name": "two.txt"
                }
            ]
        },
        {
            "type": "file",
            "name": "file1.txt"
        },
        {
            "type": "file",
            "name": "file2.txt"
        },
        {
            "type": "file",
            "name": "file3.txt"
        },
        {
            "type": "file",
            "name": "file4.txt"
        },
        {
            "type": "file",
            "name": "file5.txt"
        },
        {
            "type": "file",
            "name": "file6.txt"
        },
        {
            "type": "file",
            "name": "file7.txt"
        },
        {
            "type": "file",
            "name": "file8.txt"
        },
        {
            "type": "file",
            "name": "file9.txt"
        },
        {
            "type": "file",
            "name": "file10.txt"
        },
        {
            "type": "file",
            "name": "file11.txt"
        }
    ]
}