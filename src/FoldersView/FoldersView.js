import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Grid, Card, CardHeader, CardMedia, CardContent, Typography, CardActions, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { data } from '../JSON/json'
import ImageUploader from '../ImageUploader/ImageUploader'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        display: 'flex',
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    card: {
        marginRight: '20px',
        marginBottom: '30px',
        cursor: 'pointer'
    },
    media: {
        height: '100px',
        width: '100px',
    },
    moreIcon: {
        fontSize: '1rem'
    },
    header: {
        color: '#009879',
        paddingBottom: '10px',
        borderBottom: '1px solid #009879'
    }
}));

export default function FoldersView() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h2 className={classes.header}>File Browser</h2>
                </Grid>
                <Grid container item lg={8} md={8} xs={12} sm={6}>
                    {data.children.map((file, index) =>
                    (file.type === 'file' ? <Card key={index} className={classes.card} elevation={0}>
                        <CardMedia
                            className={classes.media}
                            image="/images/file.png"
                            title={file.name}
                        />
                        {file.name}
                    </Card> :
                        <Card key={index} className={classes.card} elevation={0}>
                            <CardMedia
                                className={classes.media}
                                image="/images/folder.png"
                                title={file.name}
                            />
                            {file.name}
                        </Card>))}
                </Grid>
                <Grid item lg={4} md={4} xs={12} sm={6}>
                    <ImageUploader />
                </Grid>
            </Grid>
        </div>
    );
}
